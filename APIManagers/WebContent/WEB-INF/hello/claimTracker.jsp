<html>
   
   <head>
      <title>Claim Tracker</title>
      <style>
         table, th , td {
            border: 1px solid grey;
            border-collapse: collapse;
            padding: 5px;
         }
         
         table tr:nth-child(odd) {
            background-color: #f2f2f2;
         }
         
         table tr:nth-child(even) {
            background-color: #ffffff;
         }
      </style>      
   </head>
   
   <body>
      <h1>Claim Tracker</h1>
      
      <div ng-app = "mainApp" ng-controller = "studentController">
         <form name = "claimForm" novalidate>
             <table border = "0">
               <tr>
                  <td>Claim no:</td>
                  <td><input name = "iClaimNo" type = "text" ng-model = "iClaimNo" required>
                     <span style = "color:red" ng-show = "claimForm.iClaimNo.$dirty && claimForm.iClaimNo.$invalid">
                        <span ng-show = "claimForm.iClaimNo.$error.required">Claim no. is required.</span>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>Insured Surname: </td>
                  <td><input name = "iSurname" type = "text" ng-model = "iSurname" required>
                     <span style = "color:red" ng-show = "claimForm.iSurname.$dirty && claimForm.iSurname.$invalid">
                        <span ng-show = "claimForm.iSurname.$error.required">Surname is required.</span>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>Date of Birth:</td>
                  <td><input name = "iDob" type = "text" ng-model = "iDob" required>
                     <span style = "color:red" ng-show = "claimForm.iDob.$dirty && claimForm.iDob.$invalid">
                        <span ng-show = "claimForm.iDob.$error.required">Date of Birth is required.</span>
                     </span>
                  </td>
               </tr>
               <tr>
                  <td>
                     <button ng-disabled = "claimForm.iClaimNo.$dirty &&
                        claimForm.iClaimNo.$invalid || claimForm.iSurname.$dirty &&
                        claimForm.iSurname.$invalid || claimForm.iDob.$dirty &&
                        claimForm.iDob.$invalid" ng-click="submit()">Enter</button>
                  </td>
               </tr>               
              </table>
         </form>
      </div>
      
      <script src = "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
      <script>
         var mainApp = angular.module("mainApp", []);
         
         mainApp.controller('studentController', function($scope) {
            $scope.reset = function(){
               $scope.iClaimNo = "claimno";
               $scope.iSurname = "surname";
               $scope.iDob = "DOB";
            }
            
            $scope.reset();
         });
      </script>      
      
   </body>
</html>