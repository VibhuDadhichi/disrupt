/**
 * This Helper class assists Aura Interface to collect and seperate data for corrosponding exchange.  
 */

package com.metlife.aura.integration.mappers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.metlife.aura.integration.businessobjects.ResponseObject;
import com.metlife.aura.integration.businessobjects.TransferObject;
import com.metlife.aura.integration.helpers.XMLHelper;
import com.rgatp.aura.wsapi.Answer;
import com.rgatp.aura.wsapi.DateQuestion;
import com.rgatp.aura.wsapi.DateWithConversionQuestion;
import com.rgatp.aura.wsapi.Entity;
import com.rgatp.aura.wsapi.EntityQuestion;
import com.rgatp.aura.wsapi.MessageResource;
import com.rgatp.aura.wsapi.QuantifiedQuestion;
import com.rgatp.aura.wsapi.Question;
import com.rgatp.aura.wsapi.RangeQuestion;
import com.rgatp.aura.wsapi.Unit;

public class AuraResponseMapper {
	private static final String PACKAGE_NAME = AuraResponseMapper.class.getPackage().getName();

	private static final String CLASS_NAME = AuraResponseMapper.class.getSimpleName();
	
	/**
	 * This method receives AURA specific Questionnaire Object (contains
	 * information related to Questions sent by AURA) and map each Question
	 * values in corrosponding TransferObject and Returns a Vector of
	 * TransferObjects of corrosponding Questions.
	 * 
	 * @param auraQuestionnaire
	 * @return
	 */

	public static List<TransferObject> populateDTO(String questionnaireId, String clientName, Question[] questions) {
		String METHOD_NAME = "populateDTO";
		
		List<TransferObject> vectorTransObj = new ArrayList<TransferObject>();
		for (int iterator = 0; iterator < questions.length; iterator++) {
			Question question = questions[iterator];
			String parentId = null;
			parentId = question.getQuestionId()+"";
			vectorTransObj.add(getTO(questionnaireId,clientName,question,parentId));			
			question = null;
		}
		
		return vectorTransObj;
	}
	
	public static TransferObject getTO(String questionnaireId, String clientName,Question question, String parentId){
		TransferObject transferObject = new TransferObject();
		if (null != question) {
			// Set Questionnaire specific attributes
			transferObject.setQuestionnaireId(questionnaireId);
			transferObject.setClientName(clientName);
			transferObject.setPartialQuestionId(parentId);
			transferObject.setTempPartialQuestionId(parentId);
			// Set Question specific attributes
			transferObject.setQuestionId(question.getQuestionId());
			transferObject.setQuestionText(question.getQuestionText());
			transferObject.setQuestionType(question.getClass().getSimpleName());
			transferObject.setQuestionNumber(question.getNumber());
			transferObject.setSubmittedAnswer(question.getAnswerText());
			transferObject.setQuestionAlias(question.getQuestionAlias());
			transferObject.setReadOnly(question.isReadOnly());
			transferObject.setBranchReadOnly(question.isBranchReadOnly());
			transferObject.setVisible(question.isVisible());
			transferObject.setBranchVisible(question.isBranchVisible());
			transferObject.setBaseQuestion(question.isBaseQuestion());
			// getting validation msgs
			if (question.getValidationMessage() != null) {
				MessageResource messageResource = null;
				messageResource = question.getValidationMessage();
				transferObject.setValidationMsg(messageResource.getText());

			}

			Answer[] answers = question.getAnswers();
			com.metlife.aura.integration.businessobjects.Answer answer;
			for (int itr = 0; itr < answers.length; itr++) {
				answer = new com.metlife.aura.integration.businessobjects.Answer();
				answer.setAccepted(answers[itr].isAccepted());
				answer.setAnswerText(answers[itr].getAnswerValue());
				
				if ((answer.getAnswerText().trim().contains("[")) && (answer.getAnswerText().trim().contains("]"))) {
					transferObject.getToolTipsKey().add(answer.getAnswerText().substring(answer.getAnswerText().indexOf("[") + ("[").length(), answer.getAnswerText()
							.lastIndexOf("]")));

				}
				
				
				
				if (null != answers[itr].getQuestions()) {
					Question[] questions = answers[itr].getQuestions();
					for (int iterAnswer = 0; iterAnswer < questions.length; iterAnswer++) {
						answer.getVectorQuestions().add(getTO(questionnaireId, clientName, questions[iterAnswer], parentId+"_"+itr+"_"+iterAnswer));
					}
					questions = null;
				}
				transferObject.getVectorAnswers().add(answer);
				answer = null;
			
				if (null != answers[itr].getQuestions()) {
					Question[] questions = answers[itr].getQuestions();
					for (int iterAnswer = 0; iterAnswer < questions.length; iterAnswer++) {
						transferObject.getVectorSubQuestions().add(getTO(questionnaireId, clientName, questions[iterAnswer], parentId+"_"+itr+"_"+iterAnswer));
					}
					questions = null;
				}
				transferObject.getListDisplayValues().add(answers[itr].getAnswerValue());
				transferObject.getMapDisplayValues().put(answers[itr].getAnswerValue(), answers[itr].getAnswerText());
			}
			populateValueListsAndMaps(question, transferObject);
		}
		
		return transferObject;
	}

	/**
	 * This Method Populates value maps of each TransferObject. This
	 * TransferObject Question specific values related coming from AURA.
	 * Question object values are mapped in the TransferObject depending upon
	 * the Question Type which is Specific to Aura Web service.
	 * 
	 * @param question
	 * @param transferObject
	 */
	@SuppressWarnings("unchecked")
	private static void populateValueListsAndMaps(Question question, TransferObject transferObject) {
		String METHOD_NAME = "populateValueListsAndMaps";
		
		if (question instanceof QuantifiedQuestion) {

			if (question instanceof RangeQuestion) {
				Unit[] units = ((QuantifiedQuestion) question).getAvailableUnits();

				for (int itrUnit = 0; itrUnit < units.length; itrUnit++) {
					MessageResource[] messageResources = units[itrUnit].getPartLabels();
					transferObject.getListValues().add(messageResources[0].getText());
					transferObject.getMapUnits().put(messageResources[0].getText(), units[itrUnit].getCode());

					for (int itrMR = 1; itrMR < messageResources.length; itrMR++) {
						transferObject.getMapUnits().put(messageResources[itrMR].getText(), units[itrUnit].getCode());
						transferObject.getListSubUnitValues().add(messageResources[itrMR].getText());
					}
				}
				units = null;
			} else if (question instanceof EntityQuestion) {
				Entity[] entities = ((EntityQuestion) question).getEntities();
				com.metlife.aura.integration.businessobjects.Entity entity;
				for (int itrEntities = 0; itrEntities < entities.length; itrEntities++) {
					entity = new com.metlife.aura.integration.businessobjects.Entity();

					entity.setName(entities[itrEntities].getName());
					entity.setAddress(entities[itrEntities].getAddress());
					entity.setContact(entities[itrEntities].getContact());

					transferObject.getVectorEntityQuestions().add(entity); // changed
					entity = null;
				}
				entities = null;

			} else if (question instanceof DateWithConversionQuestion) {
				Unit[] units = ((QuantifiedQuestion) question).getAvailableUnits();

				for (int itrUnit = 0; itrUnit < units.length; itrUnit++) {
					MessageResource[] messageResources = units[itrUnit].getPartLabels();
					transferObject.getListValues().add(messageResources[0].getText());
					transferObject.getMapUnits().put(messageResources[0].getText(), units[itrUnit].getCode());

				}
				units = null;
			}else if (question instanceof DateQuestion ) {
				Unit[] units = ((QuantifiedQuestion) question).getAvailableUnits();

				for (int itrUnit = 0; itrUnit < units.length; itrUnit++) {
					MessageResource[] messageResources = units[itrUnit].getPartLabels();
					transferObject.getListValues().add(messageResources[0].getText());
					transferObject.getMapUnits().put(messageResources[0].getText(), units[itrUnit].getCode());

					for (int itrMR = 1; itrMR < messageResources.length; itrMR++) {
						transferObject.getMapUnits().put(messageResources[itrMR].getText(), units[itrUnit].getCode());
						transferObject.getListSubUnitValues().add(messageResources[itrMR].getText());
					}
				}
				units = null;
			}
		} 
		if (question.isComplete()) {
			transferObject.setCompleted(true);
		}
	}

	/**
	 * This method populates a vector of DecisionObject which contains the
	 * Decision returned by AURA for each Cover. The decision is based on the
	 * Questionnaire responses sent to AURA.
	 * 
	 * @param outputXML
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public static ResponseObject populateResponse(String outputString) throws SAXException, IOException, ParserConfigurationException {
		String METHOD_NAME = "populateResponse";
		
		ResponseObject responseObject = null;
		if (null != outputString) {
			responseObject = XMLHelper.prepareResponse(outputString);
		}
		return responseObject;
	}
	
}