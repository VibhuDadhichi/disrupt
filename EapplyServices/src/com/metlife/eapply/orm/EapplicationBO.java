package com.metlife.eapply.orm;

import java.io.Serializable;
import java.math.BigDecimal;

public class EapplicationBO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    private Long id;
	
	private String benefit_Id;
	
	private String timeType;
	
	private BigDecimal timeType_Desc;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBenefit_Id() {
		return benefit_Id;
	}

	public void setBenefit_Id(String benefit_Id) {
		this.benefit_Id = benefit_Id;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTimeType() {
		return timeType;
	}

	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	public BigDecimal getTimeType_Desc() {
		return timeType_Desc;
	}

	public void setTimeType_Desc(BigDecimal timeType_Desc) {
		this.timeType_Desc = timeType_Desc;
	}
	
	
	
	
}
